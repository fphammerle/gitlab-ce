import Vue from 'vue';
import { GlLoadingIcon } from '@gitlab-org/gitlab-ui';

Vue.component('gl-loading-icon', GlLoadingIcon);
